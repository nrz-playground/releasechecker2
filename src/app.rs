use std::env;
use std::error::Error;

mod database;
mod docker;
mod github;
mod mattermost;
mod metrics;

#[derive(Clone)]
pub struct App {
	config: AppConfig,
	metrics_backend: metrics::MetricsBackend,
	metric_run: prometheus::Counter,
	metric_github_repository_checked: prometheus::Counter,
	metric_github_repository_tags_checked: prometheus::Counter,
	metric_docker_images_checked: prometheus::Counter,
	metric_docker_image_tags_checked: prometheus::Counter,
}

pub fn new() -> App {
	let mut a = App {
		config: new_app_config(),
		metrics_backend: metrics::new(),
		metric_run: prometheus::Counter::with_opts(prometheus::Opts::new(
			"releasechecker_runs",
			"Number of releasechecker runs",
		))
		.unwrap(),
		metric_github_repository_checked: prometheus::Counter::with_opts(prometheus::Opts::new(
			"releasechecker_github_repositories_checks",
			"Number of Github repositories checked",
		))
		.unwrap(),
		metric_github_repository_tags_checked: prometheus::Counter::with_opts(
			prometheus::Opts::new(
				"releasechecker_github_repository_tags_checks",
				"Number of Github repository tags checked",
			),
		)
		.unwrap(),
		metric_docker_images_checked: prometheus::Counter::with_opts(prometheus::Opts::new(
			"releasechecker_docker_images_checks",
			"Number of Docker images checked",
		))
		.unwrap(),
		metric_docker_image_tags_checked: prometheus::Counter::with_opts(prometheus::Opts::new(
			"releasechecker_docker_image_tags_checks",
			"Number of Docker image tags checked",
		))
		.unwrap(),
	};

	a.metrics_backend.register_counter(&a.metric_run);
	a.metrics_backend
		.register_counter(&a.metric_github_repository_checked);
	a.metrics_backend
		.register_counter(&a.metric_github_repository_tags_checked);
	a.metrics_backend
		.register_counter(&a.metric_docker_images_checked);
	a.metrics_backend
		.register_counter(&a.metric_docker_image_tags_checked);

	a
}

impl App {
	async fn process_github_releases(&mut self, mut db: database::DatabasePostgreSQL) {
		println!("Starting Github release check cron");
		match db.get_github_repositories().await {
			Ok(repos) => {
				let mut gh = github::new(&self.config.github_token);
				for repo in repos {
					self.metric_github_repository_checked.inc();

					let mut new_tags = Vec::new();
					for tag in gh.get_releases(&repo.group, &repo.repo) {
						match db
							.is_github_repository_tag_registered(&repo.group, &repo.repo, &tag)
							.await
						{
							Ok(b) => {
								if b {
									new_tags.push(tag);
								}
							}
							Err(e) => {
								println!("Unable to verify if tag is registered: {}", e);
							}
						}
						self.metric_docker_image_tags_checked.inc();
					}

					if new_tags.len() > 0 {
						// TODO: notify here

						for tag in new_tags {
							match db
								.register_github_repository_tag(&repo.group, &repo.repo, &tag)
								.await
							{
								Ok(_) => (),
								Err(e) => {
									println!(
										"Unable to register Github repository tag {}/{}:{}: {}",
										repo.group, repo.repo, tag, e
									);
								}
							}
						}
					}

					match db
						.set_github_repository_last_update(&repo.group, &repo.repo)
						.await
					{
						Ok(_) => (),
						Err(e) => {
							println!(
								"Unable to set repo last update for {}/{}: {}",
								repo.group, repo.repo, e
							)
						}
					}
				}

				println!("Github release check cron finished successfuly");
			}
			Err(e) => {
				println!("Unable to check Github releases: {}", e);
			}
		}
	}

	async fn process_dockerhub_releases(&mut self, mut db: database::DatabasePostgreSQL) {
		println!("Starting Docker release check cron");
		match db.get_dockerhub_images().await {
			Ok(images) => {
				for image in images {
					self.metric_github_repository_checked.inc();

					//let mut new_tags = Vec::new();

					// Todo: fetch docker tags

					// if new_tags.len() > 0 {
					// 	// TODO: notify here

					// 	for tag in new_tags {
					// 		match db.register_dockerhub_image_tag(&image.group, &image.image, &tag) {
					// 			Ok(_) => (),
					// 			Err(e) => {
					// 				println!(
					// 					"Unable to register Docker image tag {}/{}:{}: {}",
					// 					image.group, image.image, tag, e
					// 				);
					// 			}
					// 		}
					// 	}
					// }

					// match db.set_dockerhub_image_last_update(&image.group, &image.image) {
					// 	Ok(_) => (),
					// 	Err(e) => {
					// 		println!(
					// 			"Unable to set dockerhub image last update for {}/{}: {}",
					// 			image.group, image.image, e
					// 		)
					// 	}
					// }
				}

				println!("Docker release check cron finished successfuly");
			}
			Err(e) => {
				println!("Unable to check Docker releases: {}", e);
			}
		}
	}

	pub async fn run(&mut self) -> Result<(), Box<dyn Error>> {
		self.metric_run.inc();
		self.config.load();

		let db_conn_string = &self.config.postgres_conn_string;
		let db = database::new(db_conn_string).await?;
		let dbcs = db_conn_string.clone();
		let mut p = self.clone();

		std::thread::spawn(move || {
			let tokio_runtime = tokio::runtime::Runtime::new().unwrap();
			tokio_runtime.block_on(async {
				loop {
					match database::new(&dbcs).await {
						Ok(db) => {
							p.process_github_releases(db).await;
							std::thread::sleep(std::time::Duration::from_secs(600));
						}
						Err(e) => {
							println!("pouet33 !");
							println!("Unable to initiate DB connection to check Github releases: {}. Retrying in 1 min", e);
							std::thread::sleep(std::time::Duration::from_secs(60));
						}
					}
				}
			});
		});

		let dbcs = db_conn_string.clone();
		let mut p = self.clone();

		std::thread::spawn(move || {
			let tokio_runtime = tokio::runtime::Runtime::new().unwrap();
			tokio_runtime.block_on(async {
				loop {
					match database::new(&dbcs).await {
						Ok(db) => {
							p.process_dockerhub_releases(db).await;
							std::thread::sleep(std::time::Duration::from_secs(600));
						}
						Err(e) => {
							println!("Unable to initiate DB connection to check Dockerhub releases: {}. Retrying in 1 min", e);
							std::thread::sleep(std::time::Duration::from_secs(60));
						}
					}
				}
			});
		});

		// Database is fine, now startup app for real
		let mut mmclient = mattermost::new(
			String::from("https://hub.unix-experience.fr"),
			String::from("q9m78wdg9jbdbkna1g33rhyhey"),
			String::from("home"),
			db,
		);

		match mmclient.auth().await {
			Ok(authed) => {
				if !authed {
					std::process::exit(1);
				}
				mmclient.retrieve_team().await;
				match mmclient.run_websocket().await {
					Ok(()) => println!("Websocket ended"),
					Err(e) => println!("Websocket error: {}", e),
				}
			}
			Err(e) => {
				println!("Unable to auth to mattermost: {}", e);
			}
		}

		//self.process_dockerhub_releases(&mut db);

		Ok(())
	}
}

#[derive(Clone)]
struct AppConfig {
	postgres_conn_string: String,
	github_token: String,
	dockerhub_username: String,
	dockerhub_password: String,
}

fn new_app_config() -> AppConfig {
	AppConfig {
		dockerhub_username: String::from(""),
		dockerhub_password: String::from(""),
		github_token: String::from(""),
		postgres_conn_string: String::from("host=localhost"),
	}
}

impl AppConfig {
	fn load(&mut self) {
		match env::var("POSTGRES_CONN_STRING") {
			Ok(val) => self.postgres_conn_string = val,
			Err(_) => (),
		}

		match env::var("GITHUB_TOKEN") {
			Ok(val) => self.github_token = val,
			Err(_) => (),
		}

		match env::var("DOCKERHUB_USERNAME") {
			Ok(val) => self.dockerhub_username = val,
			Err(_) => (),
		}

		match env::var("DOCKERHUB_PASSWORD") {
			Ok(val) => self.dockerhub_password = val,
			Err(_) => (),
		}
	}
}
