use github_rs::client::{Executor, Github};
use serde_json::Value;

pub struct GithubClient {
	client: github_rs::client::Github,
}

pub fn new(token: &String) -> GithubClient {
	let c = Github::new(token).unwrap();
	GithubClient { client: c }
}

impl GithubClient {
	pub fn get_releases(&mut self, owner: &str, repo: &str) -> Vec<String> {
		let mut vres = Vec::new();

		let gh = self
			.client
			.get()
			.repos()
			.owner(owner)
			.repo(repo)
			.tags()
			.execute::<Value>();

		match gh {
			Ok((_, _, json)) => {
				if let Some(json) = json {
					match json {
						Value::Array(a) => {
							for tag in a {
								vres.push(tag["name"].to_string())
							}
						}
						_ => {
							println!("invalid json received from Github: {}", json)
						}
					}
				}
			}
			Err(e) => println!("{}", e),
		}

		vres
	}
}

pub struct GithubRepository {
	pub group: String,
	pub repo: String,
}
