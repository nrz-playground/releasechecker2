extern crate tokio;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

use std::error::Error;
use url;

use crate::app::database::DatabasePostgreSQL;

use futures_util::{SinkExt, StreamExt};
use tokio_tungstenite::{connect_async, tungstenite::protocol::Message};

#[derive(Debug, Serialize, Deserialize)]
struct User {
	id: String,
	create_at: i64,
	update_at: i64,
	delete_at: i64,
	username: String,
	auth_data: String,
	auth_service: String,
	email: String,
	nickname: String,
	first_name: String,
	last_name: String,
	position: String,
	roles: String,
	is_bot: bool,
}

#[derive(Debug, Serialize, Deserialize)]
struct AuthChallengeData {
	token: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct AuthChallenge {
	seq: i32,
	action: String,
	data: AuthChallengeData,
}

#[derive(Debug, Serialize, Deserialize)]
struct AuthWebsocketResponse {
	status: String,
	seq_reply: i64,
}

#[derive(Debug, Serialize, Deserialize)]
struct WebsocketMessageMetadata {
	seq: i32,
	event: String,
	data: serde_json::Value,
}

#[derive(Debug, Serialize, Deserialize)]
struct PostRaw {
	channel_display_name: String,
	channel_name: String,
	channel_type: String,
	mentions: Vec<String>,
	post: String,
	sender_name: String,
	set_online: bool,
	team_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Post {
	id: Option<String>,
	create_at: Option<i64>,
	update_at: Option<i64>,
	edit_at: Option<i64>,
	delete_at: Option<i64>,
	is_pinned: Option<bool>,
	user_id: Option<String>,
	channel_id: String,
	root_id: Option<String>,
	parent_id: Option<String>,
	original_id: Option<String>,
	message: String,
	#[serde(rename = "type")]
	post_type: Option<String>,
	// props: PostDataPostProps
}

pub struct Client {
	team_name: String,
	http_client: HTTPClient,
	my_id: String,
	db: DatabasePostgreSQL,
}

pub fn new(url: String, token: String, team_name: String, db: DatabasePostgreSQL) -> Client {
	Client {
		team_name: team_name,
		http_client: HTTPClient {
			url: url,
			token: token,
			client: reqwest::Client::new(),
		},
		db: db,
		my_id: String::from(""),
	}
}

impl Client {
	pub async fn auth(&mut self) -> Result<bool, Box<dyn Error>> {
		// retrieve user id to test authentication
		match self.http_client.get_me().await {
			Ok(response) => match response.status() {
				StatusCode::OK => match response.text().await {
					Ok(text) => {
						match serde_json::from_str(&text) as Result<User, serde_json::Error> {
							Ok(user) => {
								self.my_id = user.id.trim().to_string();
							}
							Err(e) => {
								println!("Unable to decode Mattermost get_me call response: {}", e)
							}
						}

						return Ok(true);
					}
					Err(e) => println!("Unable to read Mattermost get_me call response: {}", e),
				},
				s => {
					println!(
						"Unable to authenticate to mattermost: {:#?} {}",
						response, s
					);
				}
			},
			Err(e) => {
				println!("Unable to authenticate to mattermost: {}", e);
			}
		}

		Ok(false)
	}

	pub async fn run_websocket(&mut self) -> Result<(), Box<dyn Error>> {
		loop {
			println!("Connecting mattermost websocket client...");
			let connect_addr = "wss://hub.unix-experience.fr/api/v4/websocket";
			let url = url::Url::parse(&connect_addr).unwrap();

			//let (ws_tx, _) = futures_channel::mpsc::unbounded();

			let (ws_stream, _) = connect_async(url).await?;
			println!("WebSocket handshake has been successfully completed");

			let (mut write, read) = ws_stream.split();

			// create first message in order to perform authentication request
			// send authentication request
			write
				.send(Message::text(
					serde_json::to_string(&AuthChallenge {
						seq: 1,
						action: String::from("authentication_challenge"),
						data: AuthChallengeData {
							token: self.http_client.token.clone(),
						},
					})
					.unwrap(),
				))
				.await?;

			read.for_each(|message| async {
				let data = message.unwrap().to_string();
				match serde_json::from_str(&data)
					as Result<WebsocketMessageMetadata, serde_json::Error>
				{
					Ok(wmm) => {
						println!("wmm: {:#?}", wmm);

						match wmm.event.as_str() {
							"null" => {
								on_ws_null();
							}
							"hello" => {
								on_ws_hello();
							}
							"status_change" => {
								on_ws_status_change();
							}
							"typing" => {
								on_ws_typing(data);
							}
							"posted" => {
								// Decode the post message
								match wmm.data["post"].as_str() {
									Some(post) => {
										println!("Post: {:#?}", post);
										match serde_json::from_str(&post)
											as Result<Post, serde_json::Error>
										{
											Ok(post) => {
												let user_id = self.my_id.clone();
												let http_client = self.http_client.clone();
												match on_ws_posted(
													post,
													user_id,
													http_client,
													self.db.clone(),
												)
												.await
												{
													Ok(()) => println!("ok"),
													Err(e) => println!("Err: {}", e),
												}
											}
											Err(e) => {
												println!("Invalid Mattermost post data: {}", e);
												return ();
											}
										}
									}
									None => {
										return ();
									}
								}
							}
							&_ => {
								on_ws_default(wmm.event);
							}
						}
					}
					// If it's not a regular websocket event, maybe it's the auth challenge
					Err(_) => match serde_json::from_str(&data)
						as Result<AuthWebsocketResponse, serde_json::Error>
					{
						Ok(_) => (),
						Err(e) => println!(
							"Unable to decode websocket message metadata: {}. Message was: {}",
							e, data
						),
					},
				}
			})
			.await;
		}
	}

	pub async fn retrieve_team(&mut self) {
		match self
			.http_client
			.get_team_by_name(self.team_name.clone())
			.await
		{
			Ok(response) => {
				println!("{:#?}", response)
			}
			Err(e) => {
				println!("Unable to retrieve team: {}", e)
			}
		}
	}
}

struct HTTPClient {
	url: String,
	token: String,
	client: reqwest::Client,
}

impl HTTPClient {
	fn clone(&self) -> HTTPClient {
		HTTPClient {
			url: self.url.clone(),
			token: self.token.clone(),
			client: self.client.clone(),
		}
	}
	async fn get_me(&mut self) -> Result<reqwest::Response, reqwest::Error> {
		self.http_get(String::from("api/v4/users/me")).await
	}

	async fn get_team_by_name(
		&mut self,
		team_name: String,
	) -> Result<reqwest::Response, reqwest::Error> {
		self.http_get(format!("api/v4/teams/name/{}", team_name))
			.await
	}

	pub async fn post(&mut self, channel_id: &String, msg: String) -> Result<bool, Box<dyn Error>> {
		match serde_json::to_string(&Post {
			channel_id: channel_id.clone(),
			message: msg.clone(),
			create_at: None,
			delete_at: None,
			update_at: None,
			edit_at: None,
			id: None,
			is_pinned: None,
			parent_id: None,
			original_id: None,
			post_type: None,
			root_id: None,
			user_id: None,
		}) {
			Ok(s) => {
				self.http_post(String::from("api/v4/posts"), s).await?;
				Ok(true)
			}
			Err(e) => {
				println!("Unable to serialize Mattermost post: {}", e);
				Ok(false)
			}
		}
	}

	async fn http_get(&mut self, endpoint: String) -> Result<reqwest::Response, reqwest::Error> {
		self.client
			.get(format!("{}/{}", self.url, endpoint))
			.header(
				reqwest::header::AUTHORIZATION,
				format!("Bearer {}", self.token),
			)
			.send()
			.await
	}

	async fn http_post(
		&mut self,
		endpoint: String,
		body: String,
	) -> Result<reqwest::Response, reqwest::Error> {
		self.client
			.post(format!("{}/{}", self.url, endpoint))
			.header(
				reqwest::header::AUTHORIZATION,
				format!("Bearer {}", self.token),
			)
			.body(body)
			.send()
			.await
	}
}

// Websocket event handlers
fn on_ws_null() {}

fn on_ws_default(event_type: String) {
	println!("unhandled Mattermost webhook event type {}", event_type)
}

fn on_ws_hello() {}

fn on_ws_status_change() {}

fn on_ws_typing(_: String) {}

struct MattermostCommandContext {
	post_id: String,
	channel_id: String,
	args: Vec<String>,
	http_client: HTTPClient,
	db: DatabasePostgreSQL,
}

async fn on_ws_posted(
	post: Post,
	my_user_id: String,
	http_client: HTTPClient,
	db: DatabasePostgreSQL,
) -> Result<(), Box<dyn Error>> {
	let post_id = post.id.clone().unwrap();
	let user_id = post.user_id.clone().unwrap();

	if my_user_id == user_id {
		return Ok(());
	}

	// println!(
	// 	"Receive message {} on channel {} from user '{}'. Message: {}. My ID: '{}'",
	// 	post_id,
	// 	post.channel_id,
	// 	user_id,
	// 	post.message,
	// 	self.my_id,
	// );

	// Ignore non command
	if post.message.len() == 0 || post.message.chars().nth(0).unwrap() != '!' {
		println!("Message {} is not a command, ignoring", post_id);
		return Ok(());
	}

	let mut splited_iter = post.message.split_ascii_whitespace();
	let command = splited_iter.next().unwrap();

	let context = &mut MattermostCommandContext {
		channel_id: post.channel_id,
		post_id: post_id,
		args: Vec::new(),
		http_client: http_client,
		db: db,
	};

	for arg in splited_iter {
		context.args.push(String::from(arg));
	}

	match on_command(context, command).await {
		Ok(_) => Ok(()),
		Err(e) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!("Failed to process command, technical error: {}", e),
				)
				.await?;
			Err(e)
		}
	}
}

async fn on_command(
	context: &mut MattermostCommandContext,
	command: &str,
) -> Result<(), Box<dyn Error>> {
	match command {
		"!ping" => on_command_ping(context).await,
		"!github-check" => on_command_github_check(context).await,
		"!github-uncheck" => on_command_github_uncheck(context).await,
		"!docker-check" => on_command_docker_check(context).await,
		"!docker-uncheck" => on_command_docker_uncheck(context).await,
		_ => {
			println!("unknown command {}", command);
			Ok(())
		}
	}
}

async fn on_command_ping(context: &mut MattermostCommandContext) -> Result<(), Box<dyn Error>> {
	context
		.http_client
		.post(&context.channel_id, String::from("pong"))
		.await?;
	//for word in args {
	//	println!("found arg {}", word);
	//}
	Ok(())
}

async fn on_command_github_check(
	context: &mut MattermostCommandContext,
) -> Result<(), Box<dyn Error>> {
	if context.args.len() != 2 {
		context
			.http_client
			.post(
				&context.channel_id,
				String::from("Invalid command arguments. Usage !github-check <group> <repo>"),
			)
			.await?;
		return Ok(());
	}

	let registered = context
		.db
		.is_github_repository_registered(&context.args[0], &context.args[1])
		.await?;

	if registered {
		context
			.http_client
			.post(
				&context.channel_id,
				format!(
					"{}/{} github repository is already checked.",
					context.args[0], context.args[1]
				),
			)
			.await?;
		return Ok(());
	}

	match context
		.db
		.add_github_repository(&context.args[0], &context.args[1])
		.await
	{
		Ok(_) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Github repository {}/{} added to checklist.",
						context.args[0], context.args[1]
					),
				)
				.await?;
		}
		Err(e) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Unable to register check for github repository {}/{}: {}",
						context.args[0], context.args[1], e
					),
				)
				.await?;
		}
	}

	Ok(())
}

async fn on_command_docker_check(
	context: &mut MattermostCommandContext,
) -> Result<(), Box<dyn Error>> {
	if context.args.len() != 2 {
		context
			.http_client
			.post(
				&context.channel_id,
				String::from("Invalid command arguments. Usage !docker-check <group> <repo>"),
			)
			.await?;
		return Ok(());
	}

	let registered = context
		.db
		.is_dockerhub_image_registered(&context.args[0], &context.args[1])
		.await?;
	if registered {
		context
			.http_client
			.post(
				&context.channel_id,
				format!(
					"{}/{} docker image is already checked.",
					context.args[0], context.args[1]
				),
			)
			.await?;
		return Ok(());
	}

	match context
		.db
		.add_dockerhub_image(&context.args[0], &context.args[1])
		.await
	{
		Ok(_) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Docker image {}/{} added to checklist.",
						context.args[0], context.args[1]
					),
				)
				.await?;
		}
		Err(e) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Unable to register check for docker image {}/{}: {}",
						context.args[0], context.args[1], e
					),
				)
				.await?;
		}
	}

	Ok(())
}

async fn on_command_github_uncheck(
	context: &mut MattermostCommandContext,
) -> Result<(), Box<dyn Error>> {
	if context.args.len() != 2 {
		context
			.http_client
			.post(
				&context.channel_id,
				String::from("Invalid command arguments. Usage !github-uncheck <group> <image>"),
			)
			.await?;
		return Ok(());
	}

	let registered = context
		.db
		.is_github_repository_registered(&context.args[0], &context.args[1])
		.await?;
	if !registered {
		context
			.http_client
			.post(
				&context.channel_id,
				format!(
					"{}/{} github repository was not checked, ignoring.",
					context.args[0], context.args[1]
				),
			)
			.await?;
		return Ok(());
	}

	match context
		.db
		.remove_github_repository(&context.args[0], &context.args[1])
		.await
	{
		Ok(_) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Github repository {}/{} removed from checklist.",
						context.args[0], context.args[1]
					),
				)
				.await?;
		}
		Err(e) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Unable to unregister check for Github repository {}/{}: {}",
						context.args[0], context.args[1], e
					),
				)
				.await?;
		}
	}

	Ok(())
}

async fn on_command_docker_uncheck(
	context: &mut MattermostCommandContext,
) -> Result<(), Box<dyn Error>> {
	if context.args.len() != 2 {
		context
			.http_client
			.post(
				&context.channel_id,
				String::from("Invalid command arguments. Usage !docker-uncheck <group> <image>"),
			)
			.await?;
		return Ok(());
	}

	let registered = context
		.db
		.is_dockerhub_image_registered(&context.args[0], &context.args[1])
		.await?;

	if !registered {
		context
			.http_client
			.post(
				&context.channel_id,
				format!(
					"{}/{} docker image was not checked, ignoring.",
					context.args[0], context.args[1]
				),
			)
			.await?;
		return Ok(());
	}

	match context
		.db
		.remove_dockerhub_image(&context.args[0], &context.args[1])
		.await
	{
		Ok(_) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Docker image {}/{} removed from checklist.",
						context.args[0], context.args[1]
					),
				)
				.await?;
		}
		Err(e) => {
			context
				.http_client
				.post(
					&context.channel_id,
					format!(
						"Unable to unregister check for Docker image {}/{}: {}",
						context.args[0], context.args[1], e
					),
				)
				.await?;
		}
	}

	Ok(())
}
