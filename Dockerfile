FROM rust:1.59.0 as builder

COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock
COPY src src

RUN apt-get update -qyy && \
	apt-get install libssl-dev openssl
RUN cargo build --release

FROM debian:buster-slim

RUN useradd releasechecker && apt-get update -qyy && \
	apt-get install -qyy openssl ca-certificates && \
	rm -Rf /var/cache/apt/* /var/lib/dpkg/info/* /var/lib/apt/lists/*

COPY --from=builder /target/release/releasechecker /usr/bin/releasechecker

USER releasechecker
CMD ["/usr/bin/releasechecker"]
