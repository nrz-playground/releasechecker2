mod app;

#[tokio::main]
async fn main() {
	match app::new().run().await {
		Ok(_) => {}
		Err(e) => {
			println!("fatal main error: {}", e)
		}
	}
}
