use crate::app;
use bb8::{Pool, RunError};
use bb8_postgres::PostgresConnectionManager;
use std::error::Error;
use std::str::FromStr;

#[derive(Clone)]
pub struct DatabasePostgreSQL {
	pool: Pool<PostgresConnectionManager<tokio_postgres::NoTls>>,
}

pub async fn new(conn_string: &std::string::String) -> Result<DatabasePostgreSQL, Box<dyn Error>> {
	let config = tokio_postgres::config::Config::from_str(&conn_string)?;
	let manager = PostgresConnectionManager::new(config, tokio_postgres::NoTls);
	let pool = Pool::builder().max_size(10).build(manager).await?;
	Ok(DatabasePostgreSQL { pool: pool })
}

impl DatabasePostgreSQL {
	/*
	Github repositories
	*/
	pub async fn get_github_repositories(
		&mut self,
	) -> Result<Vec<app::github::GithubRepository>, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let stmt = connection
			.prepare("SELECT gh_group, gh_name FROM github_repositories")
			.await?;
		let rows = connection.query(&stmt, &[]).await?;

		let mut res = Vec::new();
		for row in rows {
			res.push(app::github::GithubRepository {
				group: row.get(0),
				repo: row.get(1),
			});
		}

		Ok(res)
	}

	pub async fn add_github_repository(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute(
			"INSERT INTO github_repositories(gh_group, gh_name) VALUES ($1, $2) ON CONFLICT ON CONSTRAINT github_repositories_pkey DO NOTHING", &[&owner, &repo]).await?;
		Ok(())
	}

	pub async fn remove_github_repository(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection
			.execute(
				"DELETE FROM github_repositories WHERE gh_group = $1 AND gh_name = $2",
				&[&owner, &repo],
			)
			.await?;
		Ok(())
	}

	pub async fn set_github_repository_last_update(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute("UPDATE github_repositories SET last_update = NOW() WHERE gh_group = $1 AND gh_name = $2", &[&owner, &repo]).await?;
		Ok(())
	}

	pub async fn register_github_repository_tag(
		&mut self,
		owner: &str,
		repo: &str,
		tag: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute(
			"INSERT INTO github_repository_tags (gh_group, gh_name, tag_name) VALUES ($1, $2, $3)",
			&[&owner, &repo, &tag],
		).await?;
		Ok(())
	}

	pub async fn is_github_repository_registered(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<bool, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let row = connection.query_one("SELECT EXISTS(SELECT 1 FROM github_repositories WHERE gh_group = $1 and gh_name = $2)", &[&owner, &repo]).await?;
		let registered: bool = row.get(0);
		Ok(registered)
	}

	pub async fn is_github_repository_tag_registered(
		&mut self,
		owner: &str,
		repo: &str,
		tag: &str,
	) -> Result<bool, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let row = connection.query_one("SELECT EXISTS(SELECT 1 FROM github_repository_tags WHERE gh_group = $1 AND gh_name = $2 AND tag_name = $3)", &[&owner, &repo, &tag]).await?;
		let registered: bool = row.get(0);
		Ok(registered)
	}

	/*
	Docker hub
	*/
	pub async fn get_dockerhub_images(
		&mut self,
	) -> Result<Vec<app::docker::DockerImage>, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let rows = connection.query("SELECT dh_group, dh_name FROM dockerhub_images WHERE last_update < NOW() - interval '1 hour'", &[]).await?;
		let mut res = Vec::new();
		for row in rows {
			res.push(app::docker::DockerImage {
				group: row.get(0),
				image: row.get(1),
			});
		}

		Ok(res)
	}

	pub async fn add_dockerhub_image(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute(
			"INSERT INTO dockerhub_image(dh_group, dh_name) VALUES ($1, $2) ON CONFLICT ON CONSTRAINT github_repositories_pkey DO NOTHING", &[&owner, &repo]).await?;
		Ok(())
	}

	pub async fn remove_dockerhub_image(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection
			.execute(
				"DELETE FROM dockerhub_image WHERE dh_group = $1 AND dh_name = $2",
				&[&owner, &repo],
			)
			.await?;
		Ok(())
	}

	pub async fn set_dockerhub_image_last_update(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute(
			"UPDATE dockerhub_images SET last_update = NOW() WHERE dh_group = $1 AND dh_name = $2",
			&[&owner, &repo],
		).await?;
		Ok(())
	}

	pub async fn register_dockerhub_image_tag(
		&mut self,
		owner: &str,
		repo: &str,
		tag: &str,
	) -> Result<(), RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		connection.execute(
			"INSERT INTO dockerhub_image_tags (gh_group, dh_name, tag_name) VALUES ($1, $2, $3)",
			&[&owner, &repo, &tag],
		).await?;
		Ok(())
	}

	pub async fn is_dockerhub_image_registered(
		&mut self,
		owner: &str,
		repo: &str,
	) -> Result<bool, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let row = connection.query_one("SELECT EXISTS(SELECT 1 FROM dockerhub_images WHERE dh_group = $1 and dh_name = $2)", &[&owner, &repo]).await?;
		let registered: bool = row.get(0);
		Ok(registered)
	}

	pub async fn is_dockerhub_image_tag_registered(
		&mut self,
		owner: &str,
		repo: &str,
		tag: &str,
	) -> Result<bool, RunError<tokio_postgres::Error>> {
		let connection = self.pool.get().await?;
		let row = connection.query_one("SELECT EXISTS(SELECT 1 FROM dockerhub_image_tags WHERE dh_group = $1 AND dh_name = $2 AND tag_name = $3)", &[&owner, &repo, &tag]).await?;
		let registered: bool = row.get(0);
		Ok(registered)
	}
}
