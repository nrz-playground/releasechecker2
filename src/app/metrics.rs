use prometheus::{Encoder, Registry, TextEncoder};

#[derive(Clone)]
pub struct MetricsBackend {
	registry: Registry,
}

pub fn new() -> MetricsBackend {
	MetricsBackend {
		registry: Registry::new(),
	}
}

impl MetricsBackend {
	pub fn register_counter(&mut self, counter: &prometheus::Counter) {
		// Create a Registry and register Counter.
		self.registry.register(Box::new(counter.clone())).unwrap();
	}

	pub fn metrics(&mut self) -> String {
		// Gather the metrics.
		let mut buffer = vec![];
		let encoder = TextEncoder::new();
		let metric_families = self.registry.gather();
		encoder.encode(&metric_families, &mut buffer).unwrap();

		String::from_utf8(buffer).unwrap()
	}
}
